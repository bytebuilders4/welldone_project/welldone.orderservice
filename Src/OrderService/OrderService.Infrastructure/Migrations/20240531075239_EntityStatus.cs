﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrderService.DataAccess.Postgre.Migrations
{
    /// <inheritdoc />
    public partial class EntityStatus : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EntityStatus",
                schema: "order",
                table: "Orders",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EntityStatus",
                schema: "order",
                table: "OrderFile",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EntityStatus",
                schema: "order",
                table: "OrderExecutor",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EntityStatus",
                schema: "order",
                table: "OrderCategory",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EntityStatus",
                schema: "order",
                table: "OrderProposal",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntityStatus",
                schema: "order",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "EntityStatus",
                schema: "order",
                table: "OrderFile");

            migrationBuilder.DropColumn(
                name: "EntityStatus",
                schema: "order",
                table: "OrderExecutor");

            migrationBuilder.DropColumn(
                name: "EntityStatus",
                schema: "order",
                table: "OrderCategory");

            migrationBuilder.DropColumn(
                name: "EntityStatus",
                schema: "order",
                table: "OrderProposal");
        }
    }
}
