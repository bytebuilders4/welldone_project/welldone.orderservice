﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.DataAccess.Postgre.EntityConfiguration
{
    public class OrderExecutorEntityTypeConfiguration : IEntityTypeConfiguration<OrderExecutor>
    {
        public void Configure(EntityTypeBuilder<OrderExecutor> orderExecutor)
        {
            orderExecutor.ToTable("OrderExecutor");

            orderExecutor.HasKey(o => o.Id);

            orderExecutor.OwnsMany(
                   order => order.TimeStamp, ownedNavigationBuilder =>
                   {
                       ownedNavigationBuilder.ToJson();
                   });

            orderExecutor.OwnsOne(
                order => order.ExecutorRef, ownedNavagationBuilder =>
                {
                    ownedNavagationBuilder.ToJson();
                });
        }
    }
}
