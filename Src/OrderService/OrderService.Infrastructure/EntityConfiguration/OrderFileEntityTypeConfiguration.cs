﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.DataAccess.Postgre.EntityConfiguration
{
    public class OrderFileEntityTypeConfiguration : IEntityTypeConfiguration<OrderFile>
    {
        public void Configure(EntityTypeBuilder<OrderFile> orderFile)
        {
            orderFile.ToTable("OrderFile");

            orderFile.HasKey(o => o.Id);

            orderFile.OwnsMany(
                   order => order.TimeStamp, ownedNavigationBuilder =>
                   {
                       ownedNavigationBuilder.ToJson();
                   });
        }
    }
}
