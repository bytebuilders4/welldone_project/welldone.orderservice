﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.DataAccess.Postgre.EntityConfiguration
{
    public class OrderProposalEntityTypeConfiguration : IEntityTypeConfiguration<OrderProposal>
    {
        public void Configure(EntityTypeBuilder<OrderProposal> orderProposal)
        {
            orderProposal.ToTable("OrderProposal");

            orderProposal.HasKey(o => o.Id);

            orderProposal.OwnsMany(
                   order => order.TimeStamp, ownedNavigationBuilder =>
                   {
                       ownedNavigationBuilder.ToJson();
                   });

            orderProposal.OwnsOne(
                order => order.DeveloperRef, ownedNavagationBuilder =>
                {
                    ownedNavagationBuilder.ToJson();
                });
        }
    }
}
