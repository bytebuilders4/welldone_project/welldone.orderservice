﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrderService.Entities;
using OrderService.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Infrastructure.EntityConfiguration
{
    public class OrderEntityTypeConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> orderConfiguration)
        {
            orderConfiguration.ToTable("Orders");

            orderConfiguration.HasKey(o => o.Id);

            orderConfiguration.OwnsMany(
                   order => order.TimeStamp, ownedNavigationBuilder =>
                   {
                       ownedNavigationBuilder.ToJson();
                   });

            orderConfiguration.HasOne(o => o.OrderCategory)
                .WithMany(o => o.Order)
                .HasForeignKey(o => o.CategoryId)
                .IsRequired(true);

            orderConfiguration.HasMany(o => o.OrderProposal)
                .WithOne(o => o.Order)
                .HasForeignKey(o => o.OrderId)
                .OnDelete(DeleteBehavior.NoAction);

            orderConfiguration.HasMany(o => o.OrderExecutor)
                .WithOne(o => o.Order)
                .HasForeignKey(o => o.OrderId)
                .OnDelete(DeleteBehavior.NoAction);

            orderConfiguration.HasMany(o => o.OrderFile)
                .WithOne(o => o.Order)
                .HasForeignKey(o => o.OrderId)
                .OnDelete(DeleteBehavior.NoAction);

            orderConfiguration.OwnsOne(
                order => order.CustomerRef, ownedNavagationBuilder =>
                {
                    ownedNavagationBuilder.ToJson();
                });
        }
    }
}
