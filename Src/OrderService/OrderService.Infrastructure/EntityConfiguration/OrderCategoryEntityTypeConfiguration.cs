﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.DataAccess.Postgre.EntityConfiguration
{
    public class OrderCategoryEntityTypeConfiguration : IEntityTypeConfiguration<OrderCategory>
    {
        public void Configure(EntityTypeBuilder<OrderCategory> orderCatConf)
        {
            orderCatConf.ToTable("OrderCategory");

            orderCatConf.HasKey(o => o.Id);
            // TODO предположительно тут надо что-то поправить :)
            orderCatConf.HasOne(o => o.Parent)
              .WithOne()
              .HasForeignKey<OrderCategory>("ParentId")
              .OnDelete(DeleteBehavior.Restrict);

            orderCatConf.Ignore(o => o.TimeStamp);

            orderCatConf.OwnsMany(
                order => order.TimeStamp, ownedNavigationBuilder =>
                {
                    ownedNavigationBuilder.ToJson();
                });
        }
    }
}
