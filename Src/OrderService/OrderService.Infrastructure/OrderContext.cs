﻿using Microsoft.EntityFrameworkCore;
using OrderService.DataAccess.Postgre.EntityConfiguration;
using OrderService.Entities;
using OrderService.Infrastructure.EntityConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Infrastructure
{
    public class OrderContext : DbContext
    {
        public OrderContext(DbContextOptions<OrderContext> options) : base(options)
        {
            Database.Migrate();
        }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderCategory> Categories { get; set; }
        public DbSet<OrderProposal> Proposals { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("order");
            builder.ApplyConfiguration(new OrderEntityTypeConfiguration());
            builder.ApplyConfiguration(new OrderCategoryEntityTypeConfiguration());
            builder.ApplyConfiguration(new OrderProposalEntityTypeConfiguration());
            builder.ApplyConfiguration(new OrderExecutorEntityTypeConfiguration());
            builder.ApplyConfiguration(new OrderFileEntityTypeConfiguration());
        }
    }
}
