﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Interfaces
{
    public interface IQuery<T> where T : class
    {
        bool NoLimit { get; set; }
        int? Offset { get; set; }
        int? Limit { get; set; }

        Expression<Func<T, bool>> GetExpression();
    }
}
