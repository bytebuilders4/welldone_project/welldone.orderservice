﻿using System.Linq.Expressions;

namespace OrderService.Interfaces
{
    public interface IRepository<T, TId> where T : class
    {
        Task<TId> Create(T item);
        Task Update(T item);
        void Delete(TId id);
        Task<T> Get(TId id);
        IQueryable<T> GetAll(IQuery<T>? query = default);
        IQueryable<T> Find(Expression<Func<T, bool>> expression);
    }
}