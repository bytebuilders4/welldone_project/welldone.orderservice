﻿using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Order, Guid> Orders { get; }
        IRepository<OrderCategory, Guid> Categories { get; }
        IRepository<OrderProposal, Guid> Proposals { get; }
        IRepository<OrderExecutor, Guid> Executors { get; }

        Task<int> SaveChangesAsync();
    }
}
