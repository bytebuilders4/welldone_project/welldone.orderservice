﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OrderService.UseCases.Category;
using OrderService.UseCases.Category.Commands.Create;
using OrderService.UseCases.Category.Commands.Delete;
using OrderService.UseCases.Category.Commands.Update;
using OrderService.UseCases.Category.Queries.GetAll;
using OrderService.UseCases.Category.Queries.GetById;

namespace OrderService.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CategoryController(ISender sender) : ControllerBase
    {

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> PostAsync([FromBody] CreateCategoryDto dto)
        {
            var category = await sender.Send(new CreateCategoryCommand() { Category = dto });
            return Created("", category);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> PutAsync(Guid id, [FromBody] CategoryDto dto)
        {
            var category = await sender.Send(new UpdateCategoryCommand() { Id = id, Category = dto });
            return Ok(category);
        }

        [HttpDelete("{Id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            await sender.Send(new DeleteCategoryCommand() { Id = id });
            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<CategoryDto>>> GetAllAsync()
        {
            var result = await sender.Send(new GetAllCategoriesQuery());
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDto>> GetById(Guid id)
        {
            var result = await sender.Send(new GetCategoryByIdQuery() { Id = id });
            if (result == null)
                return NotFound();
            return Ok(result);
        }
    }
}
