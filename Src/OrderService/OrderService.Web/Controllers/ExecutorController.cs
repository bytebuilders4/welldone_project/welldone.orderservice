﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrderService.UseCases.Executor.Commands;
using OrderService.UseCases.OrderCommands.Create;
using System.Net;
using System.Reflection;

namespace OrderService.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExecutorsController(ISender sender) : ControllerBase
    {
        [HttpPost]
        [Authorize(Roles = "Administrator,User")]
        public async Task<ActionResult<Guid>> PostAsync(Guid orderId, Guid userId, string userName)
        {
            var id = await sender.Send(new CreateExecutorCommand() 
            {  
                OrderId = orderId,
                UserRef = new Entities.UserRef()
                { 
                    UserId = userId,
                    Username = userName
                },
            });

            return new ObjectResult(id) { StatusCode = (int?)HttpStatusCode.Created };
        }
    }
}
