﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OrderService.Entities;
using OrderService.UseCases.OrderCommands.Create;
using OrderService.UseCases.OrderCommands.Delete;
using OrderService.UseCases.OrderCommands.Update;
using OrderService.UseCases.OrderCommands.UpdateOrderStatus;
using OrderService.UseCases.OrderQueries;
using OrderService.UseCases.OrderQueries.GetAll;
using OrderService.UseCases.OrderQueries.GetById;
using OrderService.Web.Utils;
using System.Net;
using System.Security.Claims;

namespace OrderService.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController(ISender sender) : ControllerBase
    {
        [HttpPost]
        [ProducesResponseType(typeof(ICollection<CreatOrderDto>), 401)]
        [Authorize]
        public async Task<ActionResult<Guid>> PostAsync([FromBody] CreatOrderDto dto)
        {
            await ConnectionLimiter.Semaphore.WaitAsync(); //we limit number orders, that created simultaneously
            var userRef = new UserRef()
            {
                UserId = Guid.Parse(HttpContext.User.FindFirst("Id")!.Value),
                Username = HttpContext.User.FindFirst(ClaimTypes.Name)!.Value
            };

            var createCommand = new CreateOrderCommand() { CreateOrderDto = dto, CustomerRef = userRef };

            var id = await sender.Send(createCommand);
            return new ObjectResult(id) { StatusCode = (int?)HttpStatusCode.Created };
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult<Guid>> PutAsync(Guid id, [FromBody] UpdateOrderDto dto)
        {
            var orderId = await sender.Send(new UpdateOrderCommand() { UpdateOrderDto = dto, Id = id });
            return Ok(orderId);
        }

        [HttpGet]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 200)]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 204)]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 206)]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 401)]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 403)]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 500)]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<GetOrderDto>>> GetAllAsync([FromQuery] OrderQuery query)
        {
            var result = await sender.Send(new GetAllOrdersQuery { Query = query });

            return result switch
            {
                _ when !result.Any() => NoContent(),
                _ when result.Count() < query.Limit => new ObjectResult(result) { StatusCode = (int?)HttpStatusCode.PartialContent },
                _ => Ok(result)
            };

        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 200)]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 401)]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 403)]
        [ProducesResponseType(typeof(ICollection<GetOrderDto>), 500)]
        //[Authorize]
        public async Task<ActionResult<GetOrderDto>> GetById(Guid id)
        {
            var result = await sender.Send(new GetOrderByIdQuery() { Id = id });
            return result != null ? Ok(result) : NotFound();
        }

        [HttpPatch("{Id}")]
        [Authorize(Roles = "Administrator,User")]
        public async Task<ActionResult<Guid>> ChangeStatusAsync(Guid id, [FromBody] UpdateOrderStatusDto dto)
        {
            var orderId = await sender.Send(new UpdateOrderStatusCommand() { Id = id, UpdateOrderStatusDto = dto });
            return Ok(orderId);
        }

        [HttpDelete("{Id}")]
        [Authorize(Roles = "Administrator,User")]
        public async Task<ActionResult<Guid>> DeleteAsync(Guid id)
        {
            await sender.Send(new DeleteOrderCommand() { Id = id });
            return NoContent();
        }
    }
}
