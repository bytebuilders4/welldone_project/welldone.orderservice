﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using OrderService.UseCases.Proposal;
using OrderService.UseCases.Proposal.Commands.Create;
using OrderService.UseCases.Proposal.Commands.Update;
using OrderService.UseCases.Proposal.Queries.GetAll;
using OrderService.UseCases.Proposal.Queries.GetById;
using OrderService.UseCases.Proposal.Queries.GetByOrderId;
using Microsoft.AspNetCore.Authorization;
using OrderService.UseCases.ProposalQueries.GetAll;

namespace OrderService.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator,User")]
    public class ProposalController(ISender sender) : ControllerBase
    {
        private readonly ISender _sender = sender;

        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] CreateProposalDto dto)
        {
            var category = await _sender.Send(new CreateProposalCommand() { Proposal = dto });
            return Created("", category);
        }

        [HttpPut]
        public async Task<ActionResult> PutAsync([FromBody] UpdateProposalDto dto)
        {
            var proposal = await _sender.Send(new UpdateProposalCommand() { Proposal = dto });
            return Ok(proposal);
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProposalDto>>> GetAllAsync([FromQuery]ProposalsQuery query)
        {
            var result = await _sender.Send(new GetAllProposalsQuery() { Query = query });
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProposalDto>> GetById(Guid id)
        {
            var result = await _sender.Send(new GetProposalByIdQuery() { Id = id });
            if (result == null)
                return NotFound();
            return Ok(result);
        }

        [HttpGet("/Order/{orderId}")]
        public async Task<ActionResult<ICollection<ProposalDto>>> GetByOrderId(Guid orderId)
        {
            var result = await _sender.Send(new GetProposalsByOrderIdQuery() { OrderId = orderId });
            if (result == null)
                return NotFound();
            return Ok(result);
        }
    }
}
