﻿namespace OrderService.Web.Utils;

public class ConnectionLimiter()
{
    public static SemaphoreSlim Semaphore = new(5);
}
