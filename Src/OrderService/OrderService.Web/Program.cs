using Microsoft.EntityFrameworkCore;
using OrderService.DataAccess.Implementaion;
using OrderService.Infrastructure;
using OrderService.Interfaces;
using OrderService.UseCases.OrderCommands.Create;
using OrderService.UseCases.Proposal.Mapping.Interface;
using OrderService.UseCases.OrderMapping.Interface;
using OrderService.Web.Utils;
using WellDone.JwtAuthConfig.Extensions;

namespace OrderService.Web;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        var connStr = builder.Configuration.GetConnectionString("OrderDB");
        builder.Services.AddControllers();
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddMemoryCache();

        builder.Services.AddExceptionHandler<GlobalExceptionHandler>();
        builder.Services.AddProblemDetails();

        builder.Services.AddJWTAuthentification();
        builder.Services.AddCustomAuthorization();

        builder.Services.AddSwaggerGen();
        builder.Services.AddDbContext<OrderContext>(opt => opt.UseNpgsql(connStr));
        builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(CreateOrderCommand).Assembly));
        builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();
        builder.Services.AddScoped<IOrderMapper, OrderMapper>();
        builder.Services.AddScoped<IProposalMapper, ProposalMapper>();

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseExceptionHandler();

        //app.UseHttpsRedirection();

        app.UseAuthentication();
        app.UseAuthorization();

        app.MapControllers();

        app.Run();
    }
}
