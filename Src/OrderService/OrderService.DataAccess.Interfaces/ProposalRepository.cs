﻿using OrderService.Entities;
using OrderService.Infrastructure;

namespace OrderService.DataAccess.Implementaion;

public class ProposalRepository(OrderContext orderContext) : Repository<OrderProposal, Guid>(orderContext);
