﻿using OrderService.Entities;
using OrderService.Infrastructure;

namespace OrderService.DataAccess.Implementaion;

public class ExecutorRepository(OrderContext orderContext) : Repository<OrderExecutor, Guid>(orderContext);
