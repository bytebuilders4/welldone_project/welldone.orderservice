﻿using OrderService.Entities;
using OrderService.Infrastructure;

namespace OrderService.DataAccess.Implementaion;

public class CategoryRepository(OrderContext orderContext) : Repository<OrderCategory, Guid>(orderContext);
