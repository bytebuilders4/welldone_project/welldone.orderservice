﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Caching.Memory;
using OrderService.Entities;
using OrderService.Entities.Enum;
using OrderService.Infrastructure;
using OrderService.Interfaces;

namespace OrderService.DataAccess.Implementaion
{
    public class UnitOfWork(OrderContext orderContext, IMemoryCache memoryCache) : IUnitOfWork
    {
        public IRepository<Order, Guid> Orders => new OrderRepository(orderContext, memoryCache);
        public IRepository<OrderCategory, Guid> Categories => new CategoryRepository(orderContext);
        public IRepository<OrderProposal, Guid> Proposals => new ProposalRepository(orderContext);
        public IRepository<OrderExecutor, Guid> Executors => new ExecutorRepository(orderContext);

        public void Dispose()
        {
            orderContext?.Dispose();
            GC.SuppressFinalize(this);
        }

        public async Task<int> SaveChangesAsync()
        {
            TrackEntities();
            var result = await orderContext.SaveChangesAsync();
            return result;
        }

        private void TrackEntities()
        {
            orderContext.ChangeTracker.DetectChanges();
            var entities = orderContext.ChangeTracker.Entries()
                                        .Where(t => t.State == EntityState.Added || t.State == EntityState.Modified)
                                        .ToArray();

            foreach (var entity in entities)
            {

                if (entity?.Entity is not ITrackable)
                    continue;
                
                var status = entity.State == EntityState.Added ? ChangeStatus.Created : ComputeChangeStatus(entity);
                var initiator = "currentUser"; // todo: read from context
                var track = entity.Entity as ITrackable;
                track?.AddTimeStamp(initiator, status);
            }
        }

        private ChangeStatus ComputeChangeStatus(EntityEntry entityEntry)
        {
            var oldSt = entityEntry.OriginalValues.GetValue<EntityStatus>("EntityStatus");
            var newSt = entityEntry.CurrentValues.GetValue<EntityStatus>("EntityStatus");

            return entityEntry.State switch
            {
                EntityState.Modified when oldSt == EntityStatus.NotActive && newSt == EntityStatus.Active => ChangeStatus.Reactivated,
                EntityState.Modified when oldSt == EntityStatus.Active && newSt == EntityStatus.NotActive => ChangeStatus.Deactivated,
                _ => ChangeStatus.Updated
            };
        }
    }
}
