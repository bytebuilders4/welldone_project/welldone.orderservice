﻿using Microsoft.EntityFrameworkCore;
using OrderService.Entities;
using OrderService.Infrastructure;
using OrderService.Interfaces;
using System.Linq.Expressions;

namespace OrderService.DataAccess.Implementaion;

public abstract class Repository<T, TId>(OrderContext orderContext) : IRepository<T, TId> where T : BaseEntity<TId>, new()
{
    protected readonly DbSet<T> _table = orderContext.Set<T>();

    public async Task<TId> Create(T item)
    {
        await _table.AddAsync(item);
        return item.Id;
    }

    public void Delete(TId id)
    {
        var item = _table.Find(id);
        if (item != null)
        {
            orderContext.Remove(item); //todo: manage dbEcxeptions
        }
    }

    public Task<T> Get(TId id)
    {
        return _table.FirstOrDefaultAsync(t => t.Id.Equals(id));
    }

    public IQueryable<T> GetAll(IQuery<T>? requestQuery = default)
    {
        if (requestQuery == null)
            return _table.AsNoTracking();

        var query = _table.AsNoTracking().Where(requestQuery.GetExpression());

        if (requestQuery.NoLimit)
            query = query.Skip((int)requestQuery.Offset!)
                         .Take((int)requestQuery.Limit!);
        return query;
    }

    public IQueryable<T> Find(Expression<Func<T, bool>> expression)
    {
        return _table.Where(expression);
    }

    public async Task Update(T item)
    {
        var dbItem = await _table.AsNoTracking().FirstOrDefaultAsync(o => o.Id.Equals(item.Id));
        if (dbItem?.TimeStamp != null)
        {
            item.TimeStamp?.AddRange(dbItem.TimeStamp);
        }
        orderContext.Attach(item).State = EntityState.Modified;
    }
}
