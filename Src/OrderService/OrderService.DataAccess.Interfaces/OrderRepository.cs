﻿using Microsoft.EntityFrameworkCore;
using OrderService.Entities;
using OrderService.Infrastructure;
using Microsoft.Extensions.Caching.Memory;

namespace OrderService.DataAccess.Implementaion;

public class OrderRepository(OrderContext db, IMemoryCache inmemoryCache) : Repository<Order, Guid>(db)
{
    public async Task<Order> Get(Guid id)
    {
        var cacheKey = GetType().Name + "_" + id;

        if (!inmemoryCache.TryGetValue(cacheKey, out Order? order))
        {
            var cacheOptions = new MemoryCacheEntryOptions()
            .SetAbsoluteExpiration(TimeSpan.FromMinutes(50))
            .SetSlidingExpiration(TimeSpan.FromMinutes(5))
            .SetPriority(CacheItemPriority.Normal);

            order = await _table.FirstOrDefaultAsync(t => t.Id.Equals(id));
            inmemoryCache.Set(cacheKey, order, cacheOptions);
        }

        return order ?? new Order();
    }
}
