﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public class OrderCategory : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public OrderCategory Parent {  get; private set; }
        public ICollection<Order> Order { get; set; }
    }
}
