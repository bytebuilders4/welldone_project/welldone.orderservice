﻿using OrderService.Entities.Enum;

namespace OrderService.Entities
{
    public class OrderProposal : BaseEntity<Guid>
    {
        public Guid OrderId { get; set; }
        public UserRef DeveloperRef { get; set; }
        public string Description { get; set; }
        public OrderProposalStatus Status { get; set; }
        public Order Order { get; set; } = null!;
    }
}
