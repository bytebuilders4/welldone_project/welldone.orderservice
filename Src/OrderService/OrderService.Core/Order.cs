﻿using OrderService.Entities.Enum;

namespace OrderService.Entities
{
    public class Order : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Guid CategoryId { get; set; }
        public UserRef CustomerRef { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;
        public DateTime DueDate { get; set; }

        public OrderCategory OrderCategory { get; set; } = null!;
        public ICollection<OrderProposal> OrderProposal { get; set; }
        public ICollection<OrderExecutor> OrderExecutor { get; set; }
        public ICollection<OrderFile> OrderFile { get; set; }
    }
}
