﻿namespace OrderService.Entities.Enum
{
    public enum OrderProposalStatus
    {
        Canceled = 0,
        Opened = 1,
        Accepted = 2,
        Declined = 3
    }
}
