﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities.Enum
{
    public enum OrderStatus
    {        
        Draft = 0,
        Published = 1,
        InProgress = 2,
        Review = 3, //contractor finished his work
        Closed = 4, //customer accepted work from contractor
        CancellationRequest = 5,
        Cancelled = 6,
        Dispute = 7
    }
}
