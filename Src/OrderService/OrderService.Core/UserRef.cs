﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public class UserRef
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
    }
}
