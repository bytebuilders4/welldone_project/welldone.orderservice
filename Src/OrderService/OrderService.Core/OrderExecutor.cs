﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public class OrderExecutor : BaseEntity<Guid>
    {
        public Guid OrderId { get; set; }
        public UserRef ExecutorRef { get; set; }
        public Order Order { get; set; } = null!;
    }
}
