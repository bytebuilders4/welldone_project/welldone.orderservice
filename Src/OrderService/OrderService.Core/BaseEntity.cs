﻿using OrderService.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public abstract class BaseEntity<T> : ITrackable
    {
        public T Id { get; set; }

        public EntityStatus EntityStatus { get; set; }
        public List<TimeStamp>? TimeStamp { get; } = [];

        public void AddTimeStamp(string initiator, ChangeStatus status)
        {
            var timeStamp = new TimeStamp()
            {
                Initiator = initiator,
                Change = status,
                ChangeTime = DateTime.UtcNow
            };
            TimeStamp.Add(timeStamp);
        }
    }
}
