﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public class OrderFile : BaseEntity<int>
    {
        public string Name { get; set; }
        public Guid OrderId { get; set; }
        public Order Order { get; set; } = null!;
    }
}