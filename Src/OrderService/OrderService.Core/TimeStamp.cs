﻿using OrderService.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public class TimeStamp
    {
        public required string Initiator { get; set; }
        public required DateTimeOffset ChangeTime { get; set; }
        public required ChangeStatus Change { get; set; }
    }

}
