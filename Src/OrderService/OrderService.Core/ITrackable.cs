﻿using OrderService.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public interface ITrackable
    {
        public List<TimeStamp>? TimeStamp { get; }

        public void AddTimeStamp(string initiator, ChangeStatus status);
    }
}
