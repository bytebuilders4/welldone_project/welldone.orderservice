﻿using MediatR;
using OrderService.Entities;
using OrderService.Interfaces;

namespace OrderService.UseCases.Executor.Commands
{
    public class CreateExecutorCommandHandler(IUnitOfWork unitOfWork) : IRequestHandler<CreateExecutorCommand, Guid>
    {
        public readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task<Guid> Handle(CreateExecutorCommand request, CancellationToken cancellationToken)
        {
            //TODO check if user is in the list of proposals
            var executor = new OrderExecutor()
            {
                OrderId = request.OrderId,
                ExecutorRef = request.UserRef
            };
            var id = await _unitOfWork.Executors.Create(executor);
            _unitOfWork.Orders.Get(id);
            await _unitOfWork.SaveChangesAsync();
            //TODO send to event queue
            
            //TODO send notificatons to proposal winner and competitors

            return id;
        }
    }
}
