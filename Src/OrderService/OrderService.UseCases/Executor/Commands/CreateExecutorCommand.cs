﻿using MediatR;
using OrderService.Entities;

namespace OrderService.UseCases.Executor.Commands;

public class CreateExecutorCommand : IRequest<Guid>
{
    public Guid OrderId { get; set; }
    public UserRef UserRef { get; set; }
}
