﻿using Mapster;
using OrderService.Entities;

namespace OrderService.UseCases.Category.Mapping.Interface
{
    [Mapper]
    public interface ICategoryMapper
    {
        public CategoryDto MapToDto(OrderCategory category);
        public ICollection<CategoryDto> MapToDto(ICollection<OrderCategory> order);
        public OrderCategory MapToEntity(CategoryDto orderDto);
    }
}
