﻿using MediatR;

namespace OrderService.UseCases.Category.Queries.GetById
{
    public class GetCategoryByIdQuery : IRequest<CategoryDto>
    {
        public required Guid Id { get; set; }
    }
}
