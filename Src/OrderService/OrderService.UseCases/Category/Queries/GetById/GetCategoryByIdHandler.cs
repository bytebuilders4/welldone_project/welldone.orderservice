﻿using Mapster;
using MediatR;
using OrderService.Interfaces;

namespace OrderService.UseCases.Category.Queries.GetById
{
    public class GetCategoryByIdHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetCategoryByIdQuery, CategoryDto>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        public async Task<CategoryDto> Handle(GetCategoryByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.Categories.Get(request.Id);
            return result.Adapt<CategoryDto>();
        }
    }
}
