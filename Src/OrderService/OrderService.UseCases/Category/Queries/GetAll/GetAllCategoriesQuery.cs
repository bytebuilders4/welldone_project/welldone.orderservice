﻿using MediatR;

namespace OrderService.UseCases.Category.Queries.GetAll
{
    public class GetAllCategoriesQuery : IRequest<IEnumerable<CategoryDto>>
    {
    }
}
