﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OrderService.Interfaces;

namespace OrderService.UseCases.Category.Queries.GetAll
{
    public class GetAllCategoriesHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetAllCategoriesQuery, IEnumerable<CategoryDto>>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        public async Task<IEnumerable<CategoryDto>> Handle(GetAllCategoriesQuery request, CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.Categories.GetAll().ToListAsync();
            return result.Adapt<IEnumerable<CategoryDto>>() ?? [];
        }
    }
}
