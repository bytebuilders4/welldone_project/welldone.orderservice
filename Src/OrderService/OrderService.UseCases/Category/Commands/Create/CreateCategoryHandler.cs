﻿
using Mapster;
using MediatR;
using OrderService.Entities;
using OrderService.Interfaces;

namespace OrderService.UseCases.Category.Commands.Create
{
    public class CreateCategoryHandler(IUnitOfWork unitOfWork) : IRequestHandler<CreateCategoryCommand, CategoryDto>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task<CategoryDto> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
        {
            var category = request.Category.Adapt<OrderCategory>();
            await _unitOfWork.Categories.Create(category);
            await _unitOfWork.SaveChangesAsync();
            return category.Adapt<CategoryDto>();
        }
    }
}