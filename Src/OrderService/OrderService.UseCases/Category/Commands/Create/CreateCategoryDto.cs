﻿namespace OrderService.UseCases.Category
{
    public class CreateCategoryDto
    {
        public required string Name { get;set; }
        public Guid? ParentId { get; set; }
    }
}
