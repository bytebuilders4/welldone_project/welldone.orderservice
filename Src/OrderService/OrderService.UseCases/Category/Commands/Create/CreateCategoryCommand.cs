﻿using MediatR;

namespace OrderService.UseCases.Category.Commands.Create
{
    public class CreateCategoryCommand : IRequest<CategoryDto>
    {
        public required CreateCategoryDto Category { get; set; }
    }
}
