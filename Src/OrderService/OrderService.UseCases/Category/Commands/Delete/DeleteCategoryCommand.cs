﻿using MediatR;

namespace OrderService.UseCases.Category.Commands.Delete
{
    public class DeleteCategoryCommand : IRequest<Guid>
    {
        public required Guid Id { get; set; }
    }
}
