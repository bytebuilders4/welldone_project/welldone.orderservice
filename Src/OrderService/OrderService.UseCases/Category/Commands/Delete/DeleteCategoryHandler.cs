﻿using MediatR;
using OrderService.Interfaces;

namespace OrderService.UseCases.Category.Commands.Delete
{
    public class DeleteCategoryHandler(IUnitOfWork unitOfWork) : IRequestHandler<DeleteCategoryCommand, Guid>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task<Guid> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            var dependentOrdersCount = _unitOfWork.Orders.Find(o => o.CategoryId == request.Id).Count();
            if (dependentOrdersCount > 0)
                throw new Exception("Есть заказы");
            var childCategoriesCount = _unitOfWork.Categories.Find(o => o.ParentId == request.Id).Count();
            if (childCategoriesCount > 0)
                throw new Exception("Есть дочерние категории");
            _unitOfWork.Categories.Delete(request.Id);
            await _unitOfWork.SaveChangesAsync();
            return request.Id;
        }
    }
}
