﻿using MediatR;

namespace OrderService.UseCases.Category.Commands.Update
{
    public class UpdateCategoryCommand : IRequest<CategoryDto>
    {
        public required CategoryDto Category { get; set; }
        public required Guid Id { get; set; }
    }
}
