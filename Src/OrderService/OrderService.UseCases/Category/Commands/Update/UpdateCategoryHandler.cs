﻿using Mapster;
using MediatR;
using OrderService.Entities;
using OrderService.Interfaces;

namespace OrderService.UseCases.Category.Commands.Update
{
    public class UpdateCategoryHandler(IUnitOfWork unitOfWork) : IRequestHandler<UpdateCategoryCommand, CategoryDto>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task<CategoryDto> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            var existingCategory = await _unitOfWork.Categories.Get(request.Id) ?? throw new Exception("Категория не найден");
            var category = request.Category.Adapt<OrderCategory>();
            await _unitOfWork.Categories.Update(category);
            await _unitOfWork.SaveChangesAsync();
            return category.Adapt<CategoryDto>();
        }
    }
}
