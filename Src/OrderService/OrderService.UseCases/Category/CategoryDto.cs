﻿using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.Category
{
    public class CategoryDto
    {
        public Guid Id { get; set; }
        public required string Name { get;set; }
        public Guid? ParentId { get; set; }

    }
}
