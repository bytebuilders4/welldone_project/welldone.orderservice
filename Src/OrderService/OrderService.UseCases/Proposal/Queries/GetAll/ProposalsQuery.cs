﻿using OrderService.Entities;
using OrderService.Entities.Enum;
using OrderService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.ProposalQueries.GetAll
{
    public class ProposalsQuery : IQuery<OrderProposal>
    {
        public bool NoLimit { get; set; }
        public int? Offset { get; set; }
        public int? Limit { get; set; }
        public Guid? OrderId { get; set; }
        public ICollection<Guid>? UserIds { get; set; }


        public Expression<Func<OrderProposal, bool>> GetExpression()
        {
            if (UserIds == null && OrderId == null)
                return user => true;

            return order => (UserIds == null || UserIds.Contains(order.DeveloperRef.UserId))
                         && (OrderId == null || OrderId.Equals(order.Id));
        }
    }
}
