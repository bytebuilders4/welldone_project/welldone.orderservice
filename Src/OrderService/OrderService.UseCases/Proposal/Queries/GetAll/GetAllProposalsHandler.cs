﻿using MediatR;
using OrderService.Interfaces;
using Microsoft.EntityFrameworkCore;
using Mapster;

namespace OrderService.UseCases.Proposal.Queries.GetAll;

public class GetAllProposalsHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetAllProposalsQuery, IEnumerable<ProposalDto>>
{
    private readonly IUnitOfWork _unitOfWork = unitOfWork;
    public async Task<IEnumerable<ProposalDto>> Handle(GetAllProposalsQuery request, CancellationToken cancellationToken)
    {
        var result = await _unitOfWork.Proposals.GetAll(request.Query).ToListAsync();
        return result.Adapt<IEnumerable<ProposalDto>>() ?? [];
    }
}
