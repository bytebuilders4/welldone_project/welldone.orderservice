﻿using MediatR;
using OrderService.UseCases.ProposalQueries.GetAll;

namespace OrderService.UseCases.Proposal.Queries.GetAll;

public class GetAllProposalsQuery : IRequest<IEnumerable<ProposalDto>>
{
    public ProposalsQuery Query { get; set; }
}
