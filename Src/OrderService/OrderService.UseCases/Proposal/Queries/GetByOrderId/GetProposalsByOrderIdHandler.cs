﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OrderService.Interfaces;

namespace OrderService.UseCases.Proposal.Queries.GetByOrderId;

public class GetProposalsByOrderIdHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetProposalsByOrderIdQuery, IEnumerable<ProposalDto>>
{
    private readonly IUnitOfWork _unitOfWork = unitOfWork;
    public async Task<IEnumerable<ProposalDto>> Handle(GetProposalsByOrderIdQuery request, CancellationToken cancellationToken)
    {
        var order = await _unitOfWork.Orders.Get(request.OrderId);
        if(order == null) { return null; }
        var result = await _unitOfWork.Proposals.Find(item => item.OrderId == request.OrderId).ToListAsync();
        return result.Adapt<IEnumerable<ProposalDto>>() ?? [];
    }
}
