﻿using MediatR;

namespace OrderService.UseCases.Proposal.Queries.GetByOrderId;

public class GetProposalsByOrderIdQuery : IRequest<IEnumerable<ProposalDto>>
{
    public required Guid OrderId { get; set; }
}
