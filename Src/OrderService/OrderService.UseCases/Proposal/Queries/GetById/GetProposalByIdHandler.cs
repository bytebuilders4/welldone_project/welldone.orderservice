﻿using MediatR;
using OrderService.Interfaces;
using Mapster;

namespace OrderService.UseCases.Proposal.Queries.GetById;

public class GetProposalByIdHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetProposalByIdQuery, ProposalDto>
{
    private readonly IUnitOfWork _unitOfWork = unitOfWork;
    public async Task<ProposalDto> Handle(GetProposalByIdQuery request, CancellationToken cancellationToken)
    {
        var result = await _unitOfWork.Proposals.Get(request.Id);
        return result.Adapt<ProposalDto>();
    }
}
