﻿using MediatR;

namespace OrderService.UseCases.Proposal.Queries.GetById;

public class GetProposalByIdQuery : IRequest<ProposalDto>
{
    public required Guid Id { get; set; }
}
