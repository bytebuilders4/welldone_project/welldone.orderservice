﻿using OrderService.Entities;
using OrderService.UseCases.Proposal.Commands.Create;
using OrderService.UseCases.Proposal.Commands.Update;
using OrderService.UseCases.Proposal.Mapping.Interface;

namespace OrderService.UseCases.Proposal.Mapping;

public class ProposalMapper : IProposalMapper
{
    public ProposalDto MapToDto(OrderProposal entity)
    {
        return new ProposalDto()
        {
            Id = entity.Id,
            OrderId = entity.OrderId,
            DeveloperRef = entity.DeveloperRef,
            Description = entity.Description,
            Status = entity.Status
        };
    }

    public ICollection<ProposalDto> MapToDto(ICollection<OrderProposal> entityList)
    {
        ICollection<ProposalDto> result = new List<ProposalDto>(entityList.Count);

        IEnumerator<OrderProposal> enumerator = entityList.GetEnumerator();

        while (enumerator.MoveNext())
        {
            OrderProposal entity = enumerator.Current;
            result.Add(new ProposalDto()
            {
                Id = entity.Id,
                OrderId = entity.OrderId,
                DeveloperRef = entity.DeveloperRef,
                Description = entity.Description,
                Status = entity.Status
            });
        }
        return result;

    }

    public OrderProposal MapToEntity(CreateProposalDto dto)
    {
        return new OrderProposal()
        {
            OrderId = dto.OrderId,
            Description = dto.Description,
            DeveloperRef = dto.DeveloperRef,
            Status = dto.Status
        };
    }

    public OrderProposal MapToEntity(UpdateProposalDto dto)
    {
        return new OrderProposal()
        {
            Id = dto.Id,
            Description = dto.Description,
            Status = dto.Status
        };
    }

    public OrderProposal MapToEntity(ProposalDto dto)
    {
        return new OrderProposal()
        {
            Id = dto.Id,
            OrderId = dto.OrderId,
            DeveloperRef = dto.DeveloperRef,
            Description = dto.Description,
            Status = dto.Status
        };
    }
}
