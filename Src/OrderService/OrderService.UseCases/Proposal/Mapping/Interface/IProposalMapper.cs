﻿using Mapster;
using OrderService.Entities;
using OrderService.UseCases.Proposal.Commands.Create;
using OrderService.UseCases.Proposal.Commands.Update;

namespace OrderService.UseCases.Proposal.Mapping.Interface;

[Mapper]
public interface IProposalMapper
{
    public ProposalDto MapToDto(OrderProposal proposal);
    public ICollection<ProposalDto> MapToDto(ICollection<OrderProposal> application);
    public OrderProposal MapToEntity(CreateProposalDto proposalDto);
    public OrderProposal MapToEntity(UpdateProposalDto proposalDto);
}
