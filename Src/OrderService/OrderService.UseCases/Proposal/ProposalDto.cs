﻿using OrderService.Entities.Enum;
using OrderService.Entities;

namespace OrderService.UseCases.Proposal;

public class ProposalDto
{
    public Guid Id { get; set; }
    public Guid OrderId { get; set; }
    public UserRef DeveloperRef { get; set; }
    public string? Description { get; set; }
    public OrderProposalStatus Status { get; set; }
}
