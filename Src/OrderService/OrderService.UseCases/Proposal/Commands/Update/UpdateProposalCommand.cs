﻿using MediatR;

namespace OrderService.UseCases.Proposal.Commands.Update;

public class UpdateProposalCommand : IRequest<ProposalDto>
{
    public required UpdateProposalDto Proposal { get; set; }
}
