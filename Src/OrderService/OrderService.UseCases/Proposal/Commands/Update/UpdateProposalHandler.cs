﻿using MediatR;
using OrderService.Interfaces;
using Mapster;
using OrderService.UseCases.Proposal.Mapping.Interface;

namespace OrderService.UseCases.Proposal.Commands.Update;

public class UpdateProposalHandler(IUnitOfWork unitOfWork, IProposalMapper mapper) : IRequestHandler<UpdateProposalCommand, ProposalDto>
{
    private readonly IProposalMapper _mapper = mapper;
    private readonly IUnitOfWork _unitOfWork = unitOfWork;

    public async Task<ProposalDto> Handle(UpdateProposalCommand request, CancellationToken cancellationToken)
    {
        var proposal = await _unitOfWork.Proposals.Get(request.Proposal.Id);
        var requestData = _mapper.MapToEntity(request.Proposal);

        if(requestData.Description != null)
            proposal.Description = requestData.Description;
        if (requestData.Status != 0)
            proposal.Status = requestData.Status;
       
        _unitOfWork.Proposals.Update(proposal);
        await _unitOfWork.SaveChangesAsync();
        return proposal.Adapt<ProposalDto>();
    }
}
