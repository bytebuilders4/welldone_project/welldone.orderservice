﻿using OrderService.Entities.Enum;

namespace OrderService.UseCases.Proposal.Commands.Update;

public class UpdateProposalDto
{
    public Guid Id { get; set; }
    public string? Description { get; set; }
    public OrderProposalStatus Status { get; set; }
}
