﻿using MediatR;
using OrderService.Interfaces;
using OrderService.UseCases.Proposal.Mapping.Interface;

namespace OrderService.UseCases.Proposal.Commands.Create;

public class CreateProposalHandler(IUnitOfWork unitOfWork, IProposalMapper mapper) : IRequestHandler<CreateProposalCommand, Guid>
{
    private readonly IProposalMapper _mapper = mapper;
    private readonly IUnitOfWork _unitOfWork = unitOfWork;

    public async Task<Guid> Handle(CreateProposalCommand request, CancellationToken cancellationToken)
    {
        var proposal = _mapper.MapToEntity(request.Proposal);
        var id = await _unitOfWork.Proposals.Create(proposal);
        await _unitOfWork.SaveChangesAsync();
        return id;
    }
}
