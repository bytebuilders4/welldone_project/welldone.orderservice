﻿using MediatR;

namespace OrderService.UseCases.Proposal.Commands.Create;

public class CreateProposalCommand : IRequest<Guid>
{
    public required CreateProposalDto Proposal { get; set; }
}
