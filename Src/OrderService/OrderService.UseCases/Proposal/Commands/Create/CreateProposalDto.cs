﻿using OrderService.Entities.Enum;
using OrderService.Entities;

namespace OrderService.UseCases.Proposal.Commands.Create;

public class CreateProposalDto
{
    public required Guid OrderId { get; set; }
    public required UserRef DeveloperRef { get; set; }
    public string? Description { get; set; }
    public OrderProposalStatus Status { get; } = OrderProposalStatus.Opened;
}
