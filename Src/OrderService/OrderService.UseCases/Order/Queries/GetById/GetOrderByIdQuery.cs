﻿using MediatR;

namespace OrderService.UseCases.OrderQueries.GetById
{
    public class GetOrderByIdQuery : IRequest<GetOrderDto>
    {
        public required Guid Id { get; set; }
    }
}
