﻿using MediatR;
using OrderService.DataAccess.Implementaion;
using OrderService.Interfaces;
using OrderService.UseCases.OrderMapping.Interface;

namespace OrderService.UseCases.OrderQueries.GetById;

public class GetOrderByIdHandler(IUnitOfWork unitOfWork, IOrderMapper orderMapper) : IRequestHandler<GetOrderByIdQuery, GetOrderDto>
{
    public async Task<GetOrderDto> Handle(GetOrderByIdQuery request, CancellationToken cancellationToken)
    {
        var result = await ((OrderRepository)unitOfWork.Orders).Get(request.Id);
        return orderMapper.MapToDto(result);
    }
}
