﻿using MediatR;
using OrderService.UseCases.OrderQueries;

namespace OrderService.UseCases.OrderQueries.GetByCustomer
{
    public class GetBycustomerQuery : IRequest<ICollection<GetOrderDto>>
    {
        public Guid CustomerId { get; set; }
    }
}
