﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using OrderService.Interfaces;
using OrderService.UseCases.OrderMapping.Interface;

namespace OrderService.UseCases.OrderQueries.GetByCustomer
{
    public class GetBycustomerQueryHandler(IUnitOfWork unitOfWork, IOrderMapper orderMapper) : IRequestHandler<GetBycustomerQuery, ICollection<GetOrderDto>>
    {
        public async Task<ICollection<GetOrderDto>> Handle(GetBycustomerQuery request, CancellationToken cancellationToken)
        {
            var result = new List<GetOrderDto>();
            var customerOrders = await unitOfWork.Orders
                .Find(o => o.CustomerRef.UserId == request.CustomerId)
                .ToListAsync(cancellationToken: cancellationToken);

            return orderMapper.MapToDto(customerOrders);
        }
    }
}
