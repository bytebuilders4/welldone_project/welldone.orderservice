﻿using OrderService.Entities.Enum;
using OrderService.Entities;

namespace OrderService.UseCases.OrderQueries;

public class GetOrderDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public Guid CategoryId { get; set; }
    public UserRef CustomerRef { get; set; }
    public OrderStatus Status { get; set; }
    public DateTime DueDate { get; set; }
    public DateTime Created { get; set; }
}
