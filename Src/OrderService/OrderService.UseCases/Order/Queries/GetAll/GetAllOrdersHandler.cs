﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using OrderService.Interfaces;
using OrderService.UseCases.OrderMapping.Interface;

namespace OrderService.UseCases.OrderQueries.GetAll;

public class GetAllOrdersHandler(IUnitOfWork unitOfWork, IOrderMapper orderMapper) : IRequestHandler<GetAllOrdersQuery, IEnumerable<GetOrderDto>>
{
    public async Task<IEnumerable<GetOrderDto>> Handle(GetAllOrdersQuery request, CancellationToken cancellationToken)
    {
        var result = await unitOfWork.Orders.GetAll(request.Query).ToListAsync();
        return orderMapper.MapToDto(result) ?? [];
    }
}
