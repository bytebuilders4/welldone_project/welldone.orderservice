﻿using MediatR;
using OrderService.Entities.Enum;

namespace OrderService.UseCases.OrderQueries.GetAll;

public class GetAllOrdersQuery : IRequest<IEnumerable<GetOrderDto>>
{
    public OrderQuery Query { get; set; }
}
