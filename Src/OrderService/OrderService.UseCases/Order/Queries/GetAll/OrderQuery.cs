﻿using OrderService.Entities;
using OrderService.Entities.Enum;
using OrderService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderQueries.GetAll
{
    public class OrderQuery : IQuery<Order>
    {
        public bool NoLimit { get; set; }
        public int? Offset { get; set; }
        public int? Limit { get; set; }

        public ICollection<OrderStatus>? Statuses { get; set; }
        public ICollection<Guid>? CustomersIds { get; set; }
        public ICollection<Guid>? CategoryIds { get; set; }


        public Expression<Func<Order, bool>> GetExpression()
        {
            if (Statuses == null && CustomersIds == null && CategoryIds == null)
                return order => true;

            return order => (Statuses == null || Statuses.Contains(order.Status))
                        && (CustomersIds == null || CustomersIds.Contains(order.CustomerRef.UserId))
                        && (CategoryIds == null || CategoryIds.Contains(order.Id));
        }
    }
}
