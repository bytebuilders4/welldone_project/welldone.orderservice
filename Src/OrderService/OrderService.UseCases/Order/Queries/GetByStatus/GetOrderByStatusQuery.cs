﻿using MediatR;
using OrderService.Entities.Enum;
using OrderService.UseCases.OrderQueries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderQueries.GetByStatus
{
    public class GetOrderByStatusQuery : IRequest<List<GetOrderDto>>
    {
        public OrderStatus OrderStatus { get; set; }
    }
}
