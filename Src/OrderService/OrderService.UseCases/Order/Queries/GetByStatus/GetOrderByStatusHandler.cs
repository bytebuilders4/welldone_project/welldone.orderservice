﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using OrderService.Entities.Enum;
using OrderService.Interfaces;
using OrderService.UseCases.OrderMapping.Interface;
using OrderService.UseCases.OrderQueries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderQueries.GetByStatus
{
    public class GetOrderByStatusHandler(IUnitOfWork unitOfWork, IOrderMapper orderMapper) : IRequestHandler<GetOrderByStatusQuery, List<GetOrderDto>>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IOrderMapper _orderMapper = orderMapper;
        public async Task<List<GetOrderDto>> Handle(GetOrderByStatusQuery request, CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.Orders.Find(o => o.Status == request.OrderStatus).ToListAsync();
            return _orderMapper.MapToDto(result).ToList();
        }
    }
}
