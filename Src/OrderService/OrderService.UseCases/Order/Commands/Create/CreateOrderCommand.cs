﻿using MediatR;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderCommands.Create
{
    public class CreateOrderCommand : IRequest<Guid>
    {
        public CreatOrderDto CreateOrderDto { get; set; }
        public UserRef CustomerRef { get; set; }
    }
}
