﻿using MediatR;
using OrderService.Interfaces;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderService.UseCases.OrderMapping.Interface;
using Mapster;
using OrderService.Entities.Enum;

namespace OrderService.UseCases.OrderCommands.Create
{
    public class CreateOrderHandler(IUnitOfWork unitOfWork, IOrderMapper orderMapper) : IRequestHandler<CreateOrderCommand, Guid>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IOrderMapper _orderMapper = orderMapper;

        public async Task<Guid> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            var order = _orderMapper.MapToEntity(request.CreateOrderDto);
            order.CustomerRef = request.CustomerRef;
            order.Status = OrderStatus.Draft;
            var id = await _unitOfWork.Orders.Create(order);
            await _unitOfWork.SaveChangesAsync();
            return id;
        }
    }
}
