﻿using MediatR;
using OrderService.UseCases.OrderQueries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderCommands.Delete
{
    public class DeleteOrderCommand : IRequest<Guid>
    {
        public required Guid Id { get; set; }
    }
}
