﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using OrderService.Entities.Enum;
using OrderService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderCommands.Delete
{
    public class DeleteOrderHandler(IUnitOfWork unitOfWork) : IRequestHandler<DeleteOrderCommand, Guid>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task<Guid> Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
        {
            var dbOrder = await _unitOfWork.Orders.Find(o => o.Id.Equals(request.Id)).FirstOrDefaultAsync() ?? throw new Exception("Заказ не найден");

            OrderStatus[] statuses = new[] { OrderStatus.Draft, OrderStatus.Published, OrderStatus.Cancelled, OrderStatus.Closed };

            if (!statuses.Contains(dbOrder.Status))
                throw new Exception("Нельзя удалять заказы  не в конечных статусах");

            dbOrder.EntityStatus = EntityStatus.NotActive;
            await _unitOfWork.Orders.Update(dbOrder);
            await _unitOfWork.SaveChangesAsync();
            return request.Id;
        }
    }
}
