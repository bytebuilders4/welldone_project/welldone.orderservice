﻿using OrderService.Entities.Enum;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderCommands.UpdateOrderStatus
{
    public class UpdateOrderStatusDto
    {
        public Guid Id { get; set; }
        public OrderStatus Status { get; set; }
    }
}
