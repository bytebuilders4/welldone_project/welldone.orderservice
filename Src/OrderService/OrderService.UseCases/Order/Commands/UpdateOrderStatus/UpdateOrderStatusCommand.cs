﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderCommands.UpdateOrderStatus
{
    public class UpdateOrderStatusCommand : IRequest<Guid>
    {
        public UpdateOrderStatusDto UpdateOrderStatusDto { get; set; }
        public Guid Id { get; set; }
    }
}
