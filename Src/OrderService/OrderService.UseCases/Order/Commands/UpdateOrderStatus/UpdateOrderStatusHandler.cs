﻿using MediatR;
using OrderService.Interfaces;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderService.UseCases.OrderMapping.Interface;
using Mapster;
using Microsoft.EntityFrameworkCore;

namespace OrderService.UseCases.OrderCommands.UpdateOrderStatus
{
    public class UpdateOrderStatusHandler(IUnitOfWork unitOfWork)
        : IRequestHandler<UpdateOrderStatusCommand, Guid>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        public async Task<Guid> Handle(UpdateOrderStatusCommand request, CancellationToken cancellationToken)
        {
            var existingOrder = await _unitOfWork.Orders.Get(request.Id) ?? throw new Exception("Заказ не найден");

            var dto = request.UpdateOrderStatusDto;
            var dbOrder = await _unitOfWork.Orders.Find(o => o.Id.Equals(dto.Id)).FirstOrDefaultAsync();
            dbOrder.Status = dto.Status;
            await _unitOfWork.Orders.Update(dbOrder);
            await _unitOfWork.SaveChangesAsync();
            return request.UpdateOrderStatusDto.Id;
        }
    }
}
