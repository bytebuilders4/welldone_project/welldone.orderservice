﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderCommands.Update
{
    public class UpdateOrderCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public UpdateOrderDto UpdateOrderDto { get; set; }
    }
}
