﻿using OrderService.Entities.Enum;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderCommands.Update
{
    public class UpdateOrderDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Guid CategoryId { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime DueDate { get; set; }
    }
}
