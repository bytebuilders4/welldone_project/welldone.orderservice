﻿using MediatR;
using OrderService.Interfaces;
using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderService.UseCases.OrderMapping.Interface;
using Mapster;
using System.ComponentModel.DataAnnotations;
using OrderService.Entities.Enum;

namespace OrderService.UseCases.OrderCommands.Update
{
    public class UpdateOrderHandler(IUnitOfWork unitOfWork, IOrderMapper orderMapper) : IRequestHandler<UpdateOrderCommand, Guid>
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IOrderMapper _orderMapper = orderMapper;

        public async Task<Guid> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
        {
            var existingOrder = await _unitOfWork.Orders.Get(request.Id) ?? throw new Exception("Заказ не найден");

            if (existingOrder.Status != OrderStatus.Draft)
                throw new Exception("Изменять описание заказа можно только до публикации");

            var order = _orderMapper.MapToEntity(request.UpdateOrderDto);
            await _unitOfWork.Orders.Update(order);
            await _unitOfWork.SaveChangesAsync();
            return order.Id;
        }
    }
}
