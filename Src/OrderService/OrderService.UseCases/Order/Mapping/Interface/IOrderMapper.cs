﻿using Mapster;
using OrderService.Entities;
using OrderService.UseCases.OrderCommands.Create;
using OrderService.UseCases.OrderCommands.Update;
using OrderService.UseCases.OrderQueries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.UseCases.OrderMapping.Interface
{
    [Mapper]
    public interface IOrderMapper
    {
        public GetOrderDto MapToDto(Order order);
        public ICollection<GetOrderDto> MapToDto(ICollection<Order> order);
        public Order MapToEntity(CreatOrderDto orderDto);
        public Order MapToEntity(UpdateOrderDto orderDto);
    }
}
