﻿using Mapster;
using OrderService.Entities;
using OrderService.UseCases.OrderQueries;

namespace OrderService.UseCases.OrderMapping.Configuration
{
    public class OrderMappingConfigRegister : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Order, GetOrderDto>()
                .Map(o => o.CustomerRef, d => d.CustomerRef);

        }
    }
}
